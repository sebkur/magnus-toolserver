<?PHP

$hide_header = true ;
$hide_doctype = true ;
include_once ( "queryclass.php") ;

$idcnt= 0 ;
$testing = isset ( $_REQUEST['test'] ) ;

$mt = microtime(true);
function ptd () { // Print Time Diff
	global $mt , $testing ;
	if ( !$testing ) return ;
	$t = microtime(true) ;
	$d = $t - $mt ;
	$mt = $t ;
	printf ( "<div>T:%2.4f</div>" , $d ) ;
}

function format_isbn ( $isbn ) {
	if ( $isbn == '' ) return $isbn ;
	if ( strlen ( $isbn ) != 13 ) return '' ;
	return substr($isbn,0,3).'-'.substr($isbn,3,1).'-'.substr($isbn,4,2).'-'.substr($isbn,6,6).'-'.substr($isbn,12,1) ;
}

function get_booksources_link ( $isbn ) {
	global $urlbase ;
	$url = $urlbase . "/Special:BookSources/" . $isbn ;
	return "<a href='$url'>BookSources</a>" ;
}

function get_pages_for_isbn ( $project , $isbn ) {
	global $db , $mysql_con , $testing ;
	make_db_safe ( $isbn ) ;
    $sql = "SELECT DISTINCT article FROM $project WHERE isbn=\"$isbn\"" ;
	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
	$ret = array() ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$ret[] = $o->article ;
#		print "<li><a href='$urlbase/" . myurlencode ( $o->article ) . "'>" . $o->article . "</a></li>" ;
	}
	asort ( $ret ) ;
	return $ret ;
}

function show_isbn_list ( $data , $nosort = 0 ) {
	global $project , $testing , $idcnt ;
    $months = get_view_months ( $project ) ;
	print "<table border=1 cellspacing=0 cellpadding=2>" ;
	print "<tr><th>Book</th><th>Cited on</th>" ;
	foreach ( $months AS $m ) print "<th>Views<br/>" . nice_month($m) . "</th>" ;
	print "</tr>" ;
	if ( $nosort == 0 ) arsort ( $data ) ;
	$viewcnt = array() ;
	foreach ( $data AS $isbn => $cnt ) {
		$gb = get_google_block ( get_isbn_meta ( $isbn ) ) ;
		print "<tr><td>".$gb."ISBN <a href='./isbn_usage.php?project=$project&show_isbn=$isbn'>" . format_isbn($isbn) . "</a> (" . get_booksources_link($isbn) . ")" ;
		if ( $cnt > 0 ) {
			$idcnt++ ;
			print " [<a href='#' onclick='toggle_pages(\"isbnu" . $idcnt . "\",\"$isbn\");return false'>Show/hide pages</a>]" ;
			print "<div id='isbnu" . $idcnt . "' style='display:none;background-color:#EEEEEE;margin:2px;padding:2px;margin-left:20px'></div>" ;
		}
		print "</td><td style='text-align:right' nowrap>$cnt pages</td>" ;
		foreach ( $months AS $m ) {
			$v = get_views($project,$isbn,$m) ;
			$viewcnt[$m] += $v ;
			print "<td align='center'>$v</td>" ;
		}
		print "</tr>" ;
		myflush() ;
	}
	
	if ( count ( $months ) > 0 ) {
		print "<tr><th colspan=2>Total views</th>" ;
		foreach ( $months AS $m ) print "<td align='center'>" . $viewcnt[$m] . "</td>" ;
		print "</tr>" ;
	}
	
	print "</table>" ;
	print "<small><i>\"Views\" refers to how often someone clicked on the ISBN link on Wikipedia to find details/order a copy of the book<br/>Book metadata provided by Google</i></small><br/>" ;
}

function filter_isbn_ranges ( $i ) {
	$ret = array() ;
	$i = explode ( "\n" , $i ) ;
	foreach ( $i AS $row ) {
		$row = trim ( $row ) ;
		if ( $row == '' ) continue ;
		$row = explode ( ' ' , $row , 2 ) ;
		$row[0] = trim ( str_replace ( '-' , '' , $row[0] ) ) ;
		if ( isset ( $row[1] ) ) $row[1] = trim ( str_replace ( '-' , '' , $row[1] ) ) ;
//		print implode ( "!" , $row ) . "<br/>" ;
		$ret[] = $row ;
	}
//	print_r ( $ret ) ;
	return $ret ;
}

function nice_month ( $m ) {
	return substr ( $m , 0 , 4 ) . '-' . substr ( $m , 4 , 2 ) ;
}

function get_views ( $project , $isbn , $month ) {
	global $db , $mysql_con , $testing ;
	make_db_safe ( $isbn ) ;
	make_db_safe ( $month ) ;
	$table = $project . '_views' ;
	$sql = "SELECT views FROM $table WHERE month='$month' AND isbn='$isbn'" ;
//	if ( $testing ) print "<div>$sql</div>" ;
	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
	$views = 0 ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$views = $o->views ;
	}
	return $views ;
}

function get_view_months ( $project ) {
	global $db , $mysql_con ;
	make_db_safe ( $isbn ) ;
	make_db_safe ( $month ) ;
	$table = $project . '_views' ;
	$sql = "SELECT DISTINCT month FROM $table" ;
	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
	$months = array() ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$months[] = $o->month ;
	}
	asort ( $months ) ;
	return $months ;
}

function get_cached_google_xml ( $isbn ) {
	global $db , $mysql_con ;
	make_db_safe ( $isbn ) ;
	$sql = "SELECT xml FROM google_cache WHERE isbn='$isbn'" ;
	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
	$xml = '' ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$xml = base64_decode ( $o->xml ) ;
	}
	if ( $xml != '' ) return $xml ; // From cache
	
	$url = "http://books.google.com/books/feeds/volumes?q=isbn:$isbn&max-results=1" ;
	$xml = file_get_contents ( $url ) ;
	
	$sql = "INSERT IGNORE INTO google_cache ( isbn , xml ) VALUES ( '$isbn' , '" . base64_encode ( $xml ) . "')" ;
	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
	
	return $xml ;
}

function get_google_isbn ( $isbn ) {
	global $testing ;
	$isbn = strtoupper ( trim ( str_replace ( '-' , '' , $isbn ) ) ) ;
	
	$txt = get_cached_google_xml ( $isbn ) ;
//	print "<pre>"; print htmlentities ( $txt ); print "</pre>"; 
	
	$ret = new DomDocument() ;	
	if ( !$ret->loadXML ( $txt ) ) return '' ;

/*	
	if ( $testing ) { # Debug data
//		$ret->formatOutput = true;
		print "<pre>" . htmlentities ( utf8_decode ( $ret->saveXML() ) ) . "</pre>" ;
		
		print "<hr/>" ;
		$n = $ret->getElementsByTagName('title');

		foreach ( $n AS $m ) {
			if ( $m->tagName != 'dc:title' ) continue ;
	//		print $m->textContent . "!<br/>" ;
		}
	}	
*/
	return $ret ;
}

function get_isbn_meta ( $isbn ) {
	return get_google_isbn ( $isbn ) ;
}

function get_google_block ( $meta ) {

	$title = '' ;
	$subtitle = '' ;
	$n = $meta->getElementsByTagName('title');
	$bad = "Search results for isbn:" ;
	foreach ( $n AS $m ) {
		$s = trim ( $m->textContent ) ;
		if ( substr ( $s , 0 , strlen ( $bad ) ) == $bad ) continue ;
		if ( $m->tagName != 'dc:title' ) {
			$title = $s ;
			continue ;
		}
		if ( $title == '' ) $title = $s ;
		else if ( $s != $title ) $subtitle = ucfirst ( $s ) ;
	}
	
	if ( $title == '' ) return '<div><i>Unknown book</i></div>' ;
	
	$author = array() ;
	$n = $meta->getElementsByTagName('creator');
	foreach ( $n AS $m ) {
		if ( $m->tagName != 'dc:creator' ) continue ;
		$author[] = trim ( $m->textContent ) ;
	}
	$author = implode ( ', ' , $author ) ;
	
	$date = '' ;
	$n = $meta->getElementsByTagName('date');
	foreach ( $n AS $m ) {
		if ( $m->tagName != 'dc:date' ) continue ;
		$date = trim ( $m->textContent ) ;
	}
	
//	return '<div>temporarily deactivated</div>' ;
	$ret = "<div>" ;
	$ret .= "<b>" . $title . "</b>" ;
	if ( $subtitle != '' ) $ret .= "<br/><i>$subtitle</i>" ;
	$ret .= "<br/>$author $date" ;
	$ret .= "</div>" ;
	return $ret ;
}

//________________________________________________________________

$doit = isset ( $_REQUEST['doit'] ) ;
$project = get_request ( 'project' , 'enwiki' ) ;
$isbns = filter_isbn_ranges ( get_request ( 'isbns' , '' ) ) ;
$show_isbn = get_request ( 'show_isbn' , '' ) ;
$action = get_request ( 'action' , '' ) ;

$isbns_text = array() ;
foreach ( $isbns AS $i ) {
	$j = array() ;
/*	if ( isset ( $i[1] ) ) {
		while ( strlen ( $i[0] ) < 13 ) $i[0] .= '0' ;
		while ( strlen ( $i[1] ) < 13 ) $i[1] .= '9' ;
	}*/
	$j[0] = format_isbn ( $i[0] ) ;
	if ( isset ( $i[1] ) ) $j[1] = format_isbn ( $i[1] ) ;
	$isbns_text[] = implode ( ' ' , $j ) ;
}
$isbns_text = implode ( "\n" , $isbns_text ) ;

$project = strtolower ( get_db_safe ( $project ) ) ;
$show_isbn = get_db_safe ( trim ( str_replace ( '-' , '' , $show_isbn ) ) ) ;

# Connect to DB
$db = 'u_magnus_isbn_p' ;
$server = "sql" ;
$user = "magnus" ;
get_database_password () ;
$password = $mysql_password ;
$mysql_con = mysql_connect ( $server , $user , $password ) ;
//$mysql_con = db_get_con() ;

$urlbase = 'http://en.wikipedia.org/wiki' ; // HACK FIXME
if ( $project == 'dewiki' ) $urlbase = 'http://de.wikipedia.org/wiki' ; // HACK FIXME

# Bot mode
if ( $action == 'isbn2articles' ) {
	$data = array() ;
	$data['isbn'] = format_isbn ( $show_isbn ) ;
	$data['pages'] = get_pages_for_isbn ( $project , $show_isbn ) ;
	
	$callback = get_request ( 'callback' , '' ) ;
	if ( $testing ) header('Content-type: text/plain; charset=utf-8');
	else header('Content-type: application/json; charset=utf-8');
	if ( $callback != '' ) print "$callback(" ;
	print json_encode ( $data ) ;
	if ( $callback != '' ) print ");" ;
	exit ( 0 ) ;
}

# Web page
header('Content-type: text/html; charset=utf-8');
print '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' . "\n\n" ;

print '<html><head>' ;
print '<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">' ;
print '<script type="text/javascript" src="lib/jquery-1.5.2.min.js"></script>' ;
print '<script type="text/javascript" src="isbn_usage.js"></script>' ;
print '<script>project="'.$project.'";urlbase="'.$urlbase.'";' ;
print '</script>' ;
print '</head><body>'  ;
print get_common_header ( "isbn_usage.php" , "ISBN usage" ) ;
print "<h1>ISBN usage</h1>" ;

print "<form method='get' action='./isbn_usage.php'>" ;
print "<table border=1 cellspacing=0 cellpadding=2>" ;

print "<tr><th>Project</th><td><select name='project'>" ;
$sql = "SELECT * FROM projects ORDER BY project" ;
$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
$data = array() ;
while ( $o = mysql_fetch_object ( $res ) ) {
	print "<option value='" . $o->project . "'" ;
	if ( $o->project == $project ) print " selected" ;
	print ">" . $o->urlbase . "</option>" ;
}
//<input type='text'name='project' value='$project' />

print "</select></td></tr>" ;

print "<tr><th>ISBN-13<br/>ranges</th><td><textarea name='isbns' cols=40 rows=5>" . $isbns_text . "</textarea>" ;
print "<br/><small><i>Enter one ISBN per line, or two separated by a space for a range</i></small></td></tr>" ;
print "<tr><td/><td><input name='doit' type='submit' value='Look up ISBN range usage' /></td></tr>" ;
print "</table>" ;
print "</form>" ;

print "<p>See also <a href='./isbn_usage.php?project=$project&action=top'>top cited books</a>, " ;
print "<a href='./isbn_usage.php?project=$project&action=topviews'>top viewed books</a>, " ;
print "<a href='./isbn_usage.php?project=$project&action=stats'>stats</a>" ;

if ( $action != '' || $show_isbn != '' || $doit ) {
	$url = "http://" . $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	$url = urlencode ( $url ) ;
	print ". Short link to this view using <a target='_blank' href=\"http://bit.ly/?u=$url\">bit.ly</a>." ;
}


print "</p>" ;
print "<hr/>" ;


function sort_by_views ( $a , $b ) {
	global $topviews ;
	if ( !isset ( $topviews[$a] ) ) return 1 ;
	if ( !isset ( $topviews[$b] ) ) return -1 ;
	return $topviews[$b] - $topviews[$a] ;
}

if ( $action == 'topviews' ) {
	$lim = 20 ;
	$offset = get_request ( 'offset' , '0' ) ;
	$ot = '' ;
	if ( $offset != '0' ) $ot = " OFFSET " . get_db_safe ( $offset ) ;

	$table = $project . "_views" ;
	$sql = "SELECT isbn,max(views) AS vc FROM $table GROUP BY isbn ORDER BY vc DESC LIMIT $lim $ot" ;
	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
	$topviews = array() ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$topviews[$o->isbn] = $o->vc ;
	}

	$isbnlist = join ( "','" , array_keys ( $topviews ) ) ;
	$sql = "SELECT isbn,count(*) AS cnt FROM $project WHERE isbn IN ('$isbnlist') GROUP BY isbn" ;
	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
	$data = array() ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$data[$o->isbn] = $o->cnt ;
	}
	
	uksort ( $data , 'sort_by_views' ) ;

	print "<h2>Most viewed books</h2><br/>" ;
	show_isbn_list ( $data , 1 ) ;
	
	$o = array() ;
	for ( $i = 0 ; $i < 10 ; $i++ ) {
		$p = $i * $lim ;
		$s = ( $p + 1 ) . "&ndash;" . ( $p + $lim ) ;
		if ( $i == $offset / $lim ) {
			$o[] = "<b>$s</b>" ;
		} else {
			$o[] = "<a href='./isbn_usage.php?project=$project&action=topviews&offset=$p'>$s</a>" ;
		}
	}
	print "<p>" . implode ( ' | ' , $o ) . "</p>" ;

} else if ( $action == 'top' ) {
	$lim = 20 ;
	$offset = get_request ( 'offset' , '0' ) ;
	$ot = '' ;
	if ( $offset != '0' ) $ot = " OFFSET " . get_db_safe ( $offset ) ;
	$sql = "SELECT isbn,count(*) AS cnt FROM $project GROUP BY isbn ORDER BY cnt DESC LIMIT $lim $ot" ;
	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
	$data = array() ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$data[$o->isbn] = $o->cnt ;
	}
	print "<h2>Most cited books</h2><br/>" ;
	show_isbn_list ( $data ) ;
	
	$o = array() ;
	for ( $i = 0 ; $i < 10 ; $i++ ) {
		$p = $i * $lim ;
		$s = ( $p + 1 ) . "&ndash;" . ( $p + $lim ) ;
		if ( $i == $offset / $lim ) {
			$o[] = "<b>$s</b>" ;
		} else {
			$o[] = "<a href='./isbn_usage.php?project=$project&action=top&offset=$p'>$s</a>" ;
		}
	}
	print "<p>" . implode ( ' | ' , $o ) . "</p>" ;

} else if ( $action == 'stats' ) {
	
	$res = my_mysql_db_query ( $db , "SELECT count(DISTINCT isbn) AS cnt FROM $project" , $mysql_con ) ;
	$o = mysql_fetch_object ( $res ) ;
	$noi = $o->cnt ;
	$res = my_mysql_db_query ( $db , "SELECT count(DISTINCT article) AS cnt FROM $project" , $mysql_con ) ;
	$o = mysql_fetch_object ( $res ) ;
	$noa = $o->cnt ;
	$res = my_mysql_db_query ( $db , "SELECT count(*) AS cnt FROM $project" , $mysql_con ) ;
	$o = mysql_fetch_object ( $res ) ;
	$nox = $o->cnt ;
	print "<h2>Stats</h2><p>$noi distinct ISBNs and $noa distinct articles in $nox individual uses.</p>" ;
	
} else if ( $show_isbn != '' ) {
    
    $meta = get_isbn_meta ( $show_isbn ) ;
    
    print "<h2>Usage of ISBN " . format_isbn($show_isbn). "</h2>" ;
    print "<p>" . get_google_block ( $meta ) . "</p>" ;
    print "<p>" . get_booksources_link ( $show_isbn ) . "</p>" ;

    $months = get_view_months ( $project ) ;
    if ( count ( $months ) > 0 ) {
	    print "<h3>BookSources views</h3>" ;
	    print "<p>How often did someone click on the ISBN link on Wikipedia to find details/order a copy?</p>" ;
		print "<table border=1 cellspacing=0 cellpadding=2><tr>" ;
	    foreach ( $months AS $m ) print "<th>" . nice_month ( $m ) . "</th>" ;
	    print "</tr><tr>" ;
	    foreach ( $months AS $m ) {
	    	print "<td align='center'>" . get_views ( $project , $show_isbn , $m ) . "</td>" ;
	    }
	    print "</tr></table>" ;
    }

	$pages = get_pages_for_isbn ( $project , $show_isbn ) ;
    print "<h3>Pages that cite this book</h3>" ;
    print "<ol>" ;
    foreach ( $pages AS $p ) {
		print "<li><a href='$urlbase/" . myurlencode ( $p ) . "'>" . $p . "</a></li>" ;
    }
    print "</ol>" ;
	
} else if ( $doit ) {

	$data = array() ;
	$total = 0 ;

	foreach ( $isbns AS $i ) {
		$isbn1 = $i[0] ;
		$isbn2 = isset ( $i[1] ) ? $i[1] : $i[0] ;
		$sql = "SELECT isbn,count(*) AS cnt FROM $project WHERE isbn BETWEEN \"$isbn1\" AND \"$isbn2\" GROUP BY isbn" ;
//		print "$isbn1 / $isbn2<br/>$sql<hr/>" ;
		$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
		while ( $o = mysql_fetch_object ( $res ) ) {
			if ( !isset ( $data[$o->isbn] ) ) $total += $o->cnt ;
			$data[$o->isbn] = $o->cnt ;
		}
	}
	
	print "<h2>ISBN range usage</h2>" ;
	print "<table border=1 cellspacing=0 cellpadding=2>" ;
	print "<tr><th>Total ISBN usages</th><td>$total</td></tr>" ;
	print "<tr><th>Distinct ISBNs used</th><td>" . count ( $data ) . "</td></tr>" ;
	print "</table><br/>" ;

	if ( $total > 0 ) {
		show_isbn_list ( $data ) ;
	}
}



ptd();

print "<p><small><i>Toolserver users: data is in database u_magnus_isbn_p</i></small></p>" ;
print "</body></html>" ;

?>