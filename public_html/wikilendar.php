<?PHP

include ( "common.php" ) ;
include_once ( "common_images.php" ) ;

$imagetypes = array () ;
$imagetypes['jpg'] = 1 ;
$imagetypes['jpeg'] = 1 ;
$imagetypes['png'] = 1 ;

load_hard_ignore_images () ;
$check_exists = false ;
$use_api = false ;
$tw = 120 ;

function load_hard_ignore_images () {
	global $hard_ignore_images ;
	$ig = file_get_contents ( 'http://meta.wikimedia.org/w/index.php?title=FIST/Ignored_images&action=raw' ) ;
	$ig = explode ( "\n" , $ig ) ;
	foreach ( $ig AS $i ) {
		if ( '*' != substr ( $i , 0 , 1 ) ) continue ;
		$i = ucfirst ( trim ( substr ( $i , 1 ) ) ) ;
		$i = str_replace ( ' ' , '_' , $i ) ;
		$hard_ignore_images['all'][] = $i ;
	}
}

function get_image ( $article ) {
	global $tw ;
	$images = db_get_image_list ( $article , 'en' , 'wikipedia' ) ;
	if ( count ( $images ) == 0 ) return '' ;
	
	while ( count ( $images ) > 0 ) {
		$ik = array_rand ( $images ) ;
		$img = $images[$ik] ;
		unset ( $images[$ik] ) ;
		$id = db_get_image_data ( $img , "en" , "wikipedia" ) ;
		if ( false === $id ) $id = db_get_image_data ( $img , "commons" , "wikimedia" ) ;
		if ( false === $id ) continue ;
		if ( $id->img_width < $tw ) continue ;
		return $img ;
	}
	return '' ;
}

function print_daily_image_link ( $dt ) {
	global $countries , $tw ;
	$url = "http://en.wikipedia.org/w/index.php?action=raw&title=" . myurlencode ( $dt ) ;
	$wiki = file_get_contents ( $url ) ;
	$wiki = explode ( "\n" , $wiki ) ;
	$lh = '_' ;
	$lines = array () ;
	$good_headings = array ( "events" , "births" , "deaths" ) ;
	$img = '' ;
	$art = '' ;
	$text = '' ;
	foreach ( $wiki AS $l ) {
		if ( "==" == substr ( $l , 0 , 2 ) ) {
			$l = strtolower ( trim ( str_replace ( "=" , "" , $l ) ) ) ;
			$lh = $l ;
			continue ;
		}
		if ( "*" != substr ( $l , 0 , 1 ) ) continue ;
		if ( !in_array ( $lh , $good_headings ) ) continue ;
		$l = ucfirst ( $lh ) . " : " . trim ( substr ( $l , 1 ) ) ;
		array_push ( $lines , $l ) ;
	}

	while ( count ( $lines ) > 0 ) {
		$k = array_rand ( $lines ) ;
		$v = $lines[$k] ;
		unset ( $lines[$k] ) ;
		$text = $v ;
		
		$parts = explode ( "[[" , $v ) ;
		array_shift ( $parts ) ;
		while ( count ( $parts ) > 0 ) {
			$pk = array_rand ( $parts ) ;
			$pv = $parts[$pk] ;
			unset ( $parts[$pk] ) ;
			$pv = array_shift ( explode ( "]]" , $pv ) ) ;
			$pv = array_shift ( explode ( "|" , $pv ) ) ;
			$pv = array_shift ( explode ( "#" , $pv ) ) ;
			$pv = ucfirst ( trim ( $pv ) ) ;
			$pv = str_replace ( " " , "_" , $pv ) ;
			if ( isset ( $countries[$pv] ) ) continue ; // No countries
			if ( preg_match ( '/^\d+$/' , $pv ) ) continue ; // No years
			$art = $pv ;
			$img = get_image ( $art ) ;
			if ( $img != '' ) break ;
		}
		
		if ( $img != '' ) break ;
	}
	
	if ( $img == '' ) {
		print "??" ;
		return ;
	}
	
	$text = str_replace ( "&ndash;" , "-" , $text ) ;
	
	$il = db_local_images ( "en" , array ( $img ) ) ;
	$purl = get_wikipedia_url ( "en" , $art ) ;
	$text = wiki2plain ( $text ) ;
	$iurl = get_thumbnail_url ( count($il)==0 ? "commons" : "en" , $img , $tw ) ;
	print "<a href='$purl'><img title='$text' src='$iurl' border='0' /></a>" ;

}

function wiki2plain ( $wiki ) {
	$url = "http://toolserver.org/~magnus/wiki2xml/w2x.php?output_format=text" ;
	$url .= "&doit=1&whatsthis=wikitext&text=" . urlencode ( $wiki ) ;
//	print "$url<br/>" ;
	return file_get_contents ( $url ) ;
}

function getMonthDays($Month, $Year)
{
   //Si la extensi�n que mencion� est� instalada, usamos esa.
   if( is_callable("cal_days_in_month"))
   {
      return cal_days_in_month(CAL_GREGORIAN, $Month, $Year);
   }
   else
   {
      //Lo hacemos a mi manera.
      return date("d",mktime(0,0,0,$Month+1,0,$Year));
   }
}

$countries = db_get_page_links ( 'List of countries by continent' , 'en' , 'wikipedia' ) ;

print "<html>" ;
print "<head></head>" ;
print "<body>" ;

$month = get_request ( 'month' , 12 ) ;
$year = get_request ( 'year' , 2009 ) ;
$dpr = 6 ;


$monthName = date("F", mktime(0, 0, 0, $month, 10)); 
$dim = getMonthDays ( $month , $year ) ;

print "<h1>Wikilendar for $monthName $year</h1>" ;
myflush();

print "<table align='center' border='1'><tr>" ;
for ( $day = 1 ; $day <= $dim ; $day++ ) {
	if ( ($day-1) % $dpr == 0 ) print "</tr><tr>" ;
	$dotw = date ( "l" , mktime(0, 0, 1, $month, $day , $year) ) ;
	print "<td align='center' valign='top'>" ;
	print "<div style='align:center'>$day $dotw</div>" ;
	print_daily_image_link ( "$monthName $day" ) ;
	print "</td>" ;
}
if ( ($day-1) % $dpr != 0 ) print "</tr>" ;
print "</table>" ;	

print "</body></html>" ;

?>