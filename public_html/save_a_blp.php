<?PHP

print '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">' ;

include ( "common.php" ) ;

function get_google_div ( $title ) {
	global $born ;
	$url = "$title -site:wikipedia.org -wikipedia" ;
	if ( isset ( $born ) ) $url = "$born $url" ;
	$url = "http://www.google.co.uk/search?q=" . urlencode ( $url ) ;
//	return "<div><a href=\"$url\" target='_blank'>Google</a></div>" ;
	return "<div width='100%'><a href=\"$url\" target='_blank'>Google</a><br/><iframe src =\"$url\" width='100%' height='300px'></iframe></div>" ;
}

function googlelize_links ( $links , $meta = array () ) {
	global $targ ;
	$ret = array () ;
	foreach ( $links AS $url ) {
		$gsim_url = "http://www.google.co.uk/search?btnG=Search&as_rq=" . urlencode ( $url ) ;
		$glink_url = "http://www.google.co.uk/search?btnG=Search&as_lq=" . urlencode ( $url ) ;
		$m = '' ;
		if ( isset ( $meta[$url] ) ) $m = ' ' . $meta[$url] ;
		$ret[$url] = "<li><a $targ href=\"$url\">$url</a> (Google <a $targ href=\"$gsim_url\">similar</a> / <a $targ href=\"$glink_url\">linkto</a>)$m</li>" ;
	}
	ksort ( $ret ) ;
	return $ret ;
}

print "<html><body>" ;
print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>' ;
print get_common_header ( "save_a_blp.php" ) ;
print "<h1>Save-a-BLP</h1>" ;

$page = get_request ( 'page' , '' ) ;

if ( $page == '' ) {
	$a = array () ;
	$pages = db_get_articles_in_category ( 'en' , 'BLP articles lacking sources' , 0 , 0 , $a , true , '' , 'wikipedia' , false ) ;
	$page = array_rand ( $pages ) ;
}


$targ = " target='_blank' " ;
$nice_title = str_replace ( '_' , ' ' , $page ) ;

# Existing categories
$cats = get_categories_of_page ( 'en' , 'wikipedia' , $nice_title ) ;
$cat_div = array () ;
foreach ( $cats AS $c => $d ) {
	$a = array () ;
	if ( preg_match ( "/^(\d+)_births$/" , $c , $a ) ) {
		$born = $a[1] ;
	}
	if ( 0 == preg_match ( "/articles/i" , $c ) ) {
		$cat_div[] = str_replace ( '_' , ' ' , $c ) ;
	}
}
if ( count ( $cat_div ) > 0 ) $cat_div = "<div><h3>Categories</h3>" . implode ( ', ' , $cat_div ) . "</div>" ;
else $cat_div = '' ;


# External links
$ext_links = get_external_links_of_page ( 'en' , 'wikipedia' , $nice_title ) ;
$ext_div = googlelize_links ( array_keys ( $ext_links ) ) ;
if ( count ( $ext_div ) > 0 ) $ext_div = "<div><h3>Existing external links</h3><ul>" . implode ( '' , $ext_div ) . "</ul></div>" ;
else $ext_div = '' ;

# Other languages ext
$ext_other = array () ;
$ext_other_meta = array () ;
$other_langs = db_get_language_links ( $nice_title , 'en' , 'wikipedia' ) ;
foreach ( $other_langs AS $lang => $ltitle ) {
	$oll = get_external_links_of_page ( $lang , 'wikipedia' , $ltitle ) ;
	foreach ( $oll AS $url => $d ) {
		if ( isset ( $ext_links[$url] ) ) continue ;
		$ext_other[$url] = $url ;
		$ext_other_meta[$url][] = $lang ;
	}
}

function other_ext_sort ( $a , $b ) {
	global $ext_other_meta_old ;
	return count ( $ext_other_meta_old[$a] ) < count ( $ext_other_meta_old[$b] ) ;
}

$ext_other_meta_old = $ext_other_meta ;
foreach ( $ext_other_meta AS $k => $v ) {
	$ext_other_meta[$k] = '[' . implode ( ', ' , $v ) . ']' ;
}

$ext_other_div = googlelize_links ( $ext_other , $ext_other_meta ) ;
uksort ( $ext_other_div , 'other_ext_sort' ) ;
if ( count ( $ext_other_div ) > 0 ) $ext_other_div = "<div><h3>External links from other languages</h3><ul>" . implode ( '' , $ext_other_div ) . "</ul></div>" ;
else $ext_other_div = '' ;


$wiki_page = 'http://en.wikipedia.org/w/index.php?action=render&title=' . urlencode ( $nice_title ) ;
$wiki_page = "<div style='float:right;padding-right:5px'><a href=\"http://en.wikipedia.org/wiki/$nice_title\" $targ>View</a> | " .
	"<a $targ href=\"http://en.wikipedia.org/w/index.php?action=edit&title=$nice_title\">Edit</a></div>" .
	"<h3>$nice_title</h3>" .
	file_get_contents ( $wiki_page ) ;
//	"<iframe src =\"$wiki_page\" width='100%' height='95%'></iframe>" ;

$top = '120' ;

$google = get_google_div ( $nice_title ) ;

print "
<div style='position:absolute;top:{$top}px;left:0.5%;right:50.5%;bottom:5px;border:1px solid black:overflow:auto'>
<div style='max-height:100%;overflow:auto'>
$wiki_page
</div>
</div>
<div style='position:absolute;top:{$top}px;right:0.5%;left:50.5%;bottom:5px;border:1px solid black'>
<div style='max-height:100%;overflow:auto;overflow-y:auto;overflow-x:hidden;padding:2px'>
$google
$cat_div
$ext_div
$ext_other_div
</div>
</div>
" ;

print "</body></html>" ;

?>
