<?PHP

include "php/common.php" ;
include "php/legacy.php" ;

$language = fix_language_code ( get_request ( 'language' , 'en' ) ) ;
$project = check_project_name ( get_request ( 'project' , 'wikipedia' ) ) ;
$title = get_request ( 'title' , '' ) ;
$summary = get_request ( 'summary' , '' ) ;
$what = get_request ( 'what' , '' ) ;
$with = get_request ( 'with' , '' ) ;
$max = get_request ( 'max' , 1000 ) ;

if ( $title == '' ) exit ;
if ( $what == '' ) exit ;
if ( $with == '' ) exit ;

$what = str_replace ( '_' , ' ' , $what ) ;
//$what = utf8_encode ( $what ) ;

$url = get_wikipedia_url ( $language , $title , "raw" , $project ) ;
$text = @file_get_contents ( $url ) ;

$text2 = str_replace ( $what , $with , $text , &$count ) ;

if ( $count == 0 ) {
	$text2 = str_replace ( str_replace ( ' ' , '_' , $what ) , $with , $text , &$count ) ;
}

if ( $count == 0 && substr ( $what , 0 , 1 ) == ':'  && substr ( $with , 0 , 1 ) == ':' ) {
	$text2 = str_replace ( substr ( $what , 1 ) , substr ( $with , 1 ) , $text , &$count ) ;
}

if ( $count == 0 && substr ( $what , 0 , 1 ) == ':'  && substr ( $with , 0 , 1 ) == ':' ) {
	$text2 = str_replace ( substr ( str_replace ( ' ' , '_' , $what ) , 1 ) , substr ( $with , 1 ) , $text , &$count ) ;
}

if ( $count > $max ) {
	print "$count replacements, $max maximum. No can do." ;
}

if ( $count == 0 ) {
	print "Could not find a match for $what." ;

print "$text" ;
	exit ;
}

$button = cGetEditButton ( htmlspecialchars ( $text2 ) , $title , $language , $project , "$what => $with$summary" , 'Show diff' , false , false , true , true ) ;

print "<html><head></head>" ;
print "<body onload=\"document.forms[0].submit();\" >" ;
print "$count occurrence(s) found. Diff should be loaded now; if not, turn on JavaScript or click the button yourself.<br/>$button<br/>" ;
print "</body></html>\n" ;
myflush();

?>