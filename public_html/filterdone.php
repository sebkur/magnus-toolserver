<?php

error_reporting ( E_ALL ) ;

include_once ( "queryclass.php") ;
$is_on_toolserver = false ;

function print_menu () {
	global $project , $language , $title ;
	print "<form method='get'>
	<table>
	<tr><th>Project</th><td><input type='text' name='project' value='{$project}'/></td></tr>
	<tr><th>Language</th><td><input type='text' name='language' value='{$language}'/></td></tr>
	<tr><th>Article</th><td><input type='text' name='title' value='{$title}'/></td></tr>
	<tr><th></th><td><input type='submit' name='doit' value='Run'/></td></tr>
	</table></form>" ;
}

function run () {
	global $project , $language , $title ;
	$text = get_wikipedia_article ( $language , $title , false , $project ) ;
	$lines = explode ( "\n" , $text ) ;
	$out = array () ;
	$mysql_con = db_get_con_new($language,$project) ;
	$db = $language . 'wiki_p' ;


	foreach ( $lines AS $l ) {
		if ( substr ( $l , 0 , 1 ) != '*' && substr ( $l , 0 , 1 ) != '#' ) {
			$out[] = $l ;
			continue ;
		}
		
		$link = array_pop ( explode ( "[[" , $l , 2 ) ) ;
		$link = array_shift ( explode ( "]]" , $link , 2 ) ) ;
		$link = array_shift ( explode ( "|" , $link , 2 ) ) ;
		make_db_safe ( $link ) ;
		$sql = "SELECT * FROM page WHERE page_namespace=0 AND page_title=\"$link\"" ;
		$res = mysql_db_query ( $db , $sql , $mysql_con ) ;
		if ( $o = mysql_fetch_object ( $res ) ) {
		} else {
			$out[] = $l ;
		}
		mysql_free_result ( $res ) ;
		
	}
	
	print "<textarea style='width:100%' rows='20'>" ;
	print implode ( "\n" , $out ) ;
	print "</textarea>" ;
}

$language = fix_language_code ( get_request ( 'language' , 'de' ) ) ;
$project = check_project_name ( get_request ( 'project' , 'wikipedia' ) ) ;
$title = get_request ( 'title' ) ;

print "<html><body>" ;
print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>' ;
print get_common_header ( "filterdone.php" ) ;
print "<h1>Filter done</h1>" ;
print "<p>Filters a wiki page that contains a list of article links and removes the ones that exist.<br/>
<i>Note :</i> This will only work for links in the main (article/gallery) namespace.</p>" ;

print_menu () ;

if ( isset ( $_REQUEST['doit'] ) && $title != '' ) run () ;


print "</body></html>" ;

?>
