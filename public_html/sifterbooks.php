<?PHP

include "common.php" ;

$partners = array () ;
$partners['eol'] = array ( 'Encyclopedia of Life' , 'Hemigrapsus penicillatus by OpenCage.jpg' , '#AAFD8E' , 'EOL/Wikipedia book' ) ;
$partners['rfam'] = array ( 'Rfam' , 'RF00629.jpg' , '' , 'Rfam/Wikipedia book' ) ;
$partners['pfam'] = array ( 'Pfam' , 'Protein.png' , '' , 'Pfam/Wikipedia book' ) ;


print '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head><body>' ;
print get_common_header ( "sifterbooks.php" , "Sifter books" ) ;
print "<h1>Sifter books</h1>" ;
print "<p>Trusted Wikipedia articles/revisions, reviewed by our partners, for book generation.</p>" ;

$splitbooks = get_request ( 'splitbooks' , 100 ) ;
$partner = strtolower ( get_request ( 'partner' , '' ) ) ;
if ( !isset ( $partners[$partner] ) ) {
	print "<form method='get' action='sifterbooks.php'><table>
	<tr><th>Partner</th><td><select name='partner'>" ;
	
	$first = "checked" ;
	foreach ( $partners AS $k => $v ) {
		print "<option value='$k' $first>" . $v[0] . "</option>" ;
		$first = '' ;
	}
	
//	<input type='text' name='partner' />
	print "</select></td></tr>
	<tr><th>Articles per book</th><td><input type='number' name='splitbooks' value='$splitbooks' /></td></tr>
	<tr><th/><td><input type='submit' name='doit' /></td></tr>
	</table></form>" ;
	
//	print "Add ?partner=??? (and maybe ) to the URL</body></html>" ;
	exit ( 0 ) ;
}

$partner_title = $partners[$partner][0] ;
$cover_image = $partners[$partner][1] ;
$cover_color = $partners[$partner][2] ;
$booktitle = $partners[$partner][3] ;

print "<textarea rows='50' style='width:100%'>{{saved book
 |title=$partner_title
 |subtitle=Trusted Wikipedia articles/revisions
 |cover-image=$cover_image
 |cover-color=$cover_color
}}
" ;


$mysql_con = db_get_con() ;
if ( !isset ( $mysql_con ) ) { print "MySQL problem" ; exit ( 0 ) ; }

$partner = strtolower ( get_db_safe ( $partner ) ) ;

$sql = "SELECT * FROM sifted WHERE partner=\"$partner\" ORDER BY wikipage" ;
$res = @my_mysql_db_query ( "u_magnus_sifter_p" , $sql , $mysql_con ) ;
if ( mysql_errno() != 0 ) print "BUG FIXME" ;
$reviewers = array () ;
$cnt = 0 ;
while ( $o = mysql_fetch_object ( $res ) ) {
	if ( $cnt % $splitbooks == 0 ) {
		$bn = $cnt / $splitbooks + 1 ;
		if ( $bn == 1 ) $bn = '' ;
		print "\n\n== $booktitle $bn ==\n" ;
	}
	$cnt++ ;
	$t = str_replace ( '_' , ' ' , $o->wikipage ) ;
	$rev = $o->revision ;
	$t2 = array_shift ( explode ( ' (' , $t ) ) ;
	print ":[{{fullurl:$t|oldid=$rev}} $t2]\n" ;
	$reviewers[$o->reviewer][] = $t2 ;
}

print "----\nReviewers:\n" ;
if ( count ( $reviewers ) == 1 ) {
	foreach ( $reviewers AS $r => $list ) {
		print ";$r : " . count ( $list ) . " articles (all)\n" ;
	}
} else {
	foreach ( $reviewers AS $r => $list ) {
		print ";$r : " . count ( $list ) . " articles (" . implode ( ', ' , $list ) . ")\n" ;
	}
}

print "</textarea>" ;

print '</body></html>' ;
?>
