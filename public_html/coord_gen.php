<?PHP

include ( 'common.php' ) ;

function prefilled_input ( $key , $type = 'text' , $value = '' , $text = '' ) {
	if ( $type == 'text' ) {
		$ret = "<input type='text' name='{$key}' size='4' value='" . get_request ( $key , '' ) . "' />" ;
	} else if ( $type == 'radio' ) {
		if ( get_request ( $key , $value ) == $value ) $check = 'checked' ;
		else $check = '' ;
		$ret = "<input type='radio' name='{$key}' value='{$value}' {$check} />" ;
	} else if ( $type == 'option' ) {
		if ( get_request ( $key , $value ) == $value ) $check = 'selected' ;
		else $check = '' ;
		$ret = "<option value='{$value}' {$check} >{$text}</option>" ;
	}
	return $ret ;
}

function process_form () {
	if ( !isset ( $_REQUEST['doit'] ) ) return '' ;
	
	$breite_grad = intval ( get_request ( 'breite_grad' , '' ) ) ;
	$breite_minute = intval ( get_request ( 'breite_minute' , '' ) ) ;
	$breite_sekunde = intval ( get_request ( 'breite_sekunde' , '' ) ) ;
	$breite_dir = get_request ( 'breite_dir' , '' ) ;
	$laenge_grad = intval ( get_request ( 'laenge_grad' , '' ) ) ;
	$laenge_minute = intval ( get_request ( 'laenge_minute' , '' ) ) ;
	$laenge_sekunde = intval ( get_request ( 'laenge_sekunde' , '' ) ) ;
	$laenge_dir = get_request ( 'laenge_dir' , '' ) ;
	$data_type = get_request ( 'data_type' , '' ) ;
	$data_size = get_request ( 'data_size' , '' ) ;
	$template_name = get_request ( 'template_name' , '' ) ;
/*	
	$breite_grad = substr ( '000' . $breite_grad , -3 , 3 ) ;
	$breite_minute = substr ( '000' . $breite_minute , -2 , 2 ) ;
	$breite_sekunde = substr ( '000' . $breite_sekunde , -2 , 2 ) ;
	$laenge_grad = substr ( '000' . $laenge_grad , -3 , 3 ) ;
	$laenge_minute = substr ( '000' . $laenge_minute , -2 , 2 ) ;
	$laenge_sekunde = substr ( '000' . $laenge_sekunde , -2 , 2 ) ;
	
	while ( substr ( $breite_grad , 0 , 1 ) == '0' ) $breite_grad = substr ( $breite_grad , 1 ) ;
	while ( substr ( $laenge_grad , 0 , 1 ) == '0' ) $laenge_grad = substr ( $laenge_grad , 1 ) ;
*/	
	$dirs_d = array ( 'N' => 'N' , 'S' => 'S' , 'E' => 'O' , 'W' => 'W' ) ;
	$breite_dir_d = $dirs_d[$breite_dir] ;
	$laenge_dir_d = $dirs_d[$laenge_dir] ;
	
	if ( $data_type == 'city' && $data_size != '' ) $extra_param = "pop={$data_size}|" ;
	elseif ( $data_type == 'mountain' && $data_size != '' ) $extra_param = "elevation={$data_size}|" ;
	else $extra_param = "" ;
	
	$text1 = "{$breite_grad}° {$breite_minute}′ {$breite_sekunde}″ {$breite_dir_d}, {$laenge_grad}° {$laenge_minute}′ {$laenge_sekunde}″ {$laenge_dir_d}" ;
	$text2 = "{$breite_grad}° {$breite_minute}′ {$breite_dir_d}, {$laenge_grad}° {$laenge_minute}′ {$laenge_dir_d}" ;
	$text3 = "{$breite_grad}° {$breite_dir_d}, {$laenge_grad}° {$laenge_dir_d}" ;

	$key1 = "<tr><td>Grad Minuten Sekunden</td><td>
{{Coordinate|{$template_name}NS={$breite_grad}/{$breite_minute}/{$breite_sekunde}/{$breite_dir}|EW={$laenge_grad}/{$laenge_minute}/{$laenge_sekunde}/{$laenge_dir}|type={$data_type}|${extra_param}region=…}}</td><td>{$text1}</td></tr>" ;

	$key2 = "<tr><td>Grad Minuten</td><td>
{{Coordinate|{$template_name}NS={$breite_grad}/{$breite_minute}//{$breite_dir}|EW={$laenge_grad}/{$laenge_minute}//{$laenge_dir}|type={$data_type}|${extra_param}region=…}}</td><td>{$text2}</td></tr>" ;

	$key3 = "<tr><td>Grad</td><td>
{{Coordinate|{$template_name}NS={$breite_grad}///{$breite_dir}|EW={$laenge_grad}///{$laenge_dir}|type={$data_type}|${extra_param}region=…}}</td><td>{$text3}</td></tr>" ;
	
	$ret = "<table border='1' cellspacing='0' cellpadding='2'><tr><th>Details</th><th>Vorlage zum Kopieren</th><th>Vorschau</th></tr>" ;
	if ( $breite_minute != '' && $laenge_minute != '' ) {
		if ( $breite_sekunde != '' && $laenge_sekunde != '' ) {
			$ret .= $key1 ;
		}
		$ret .= $key2 ;
	}
	$ret .= $key3 ;


	$ret .= "</table><hr/>" ;

	return $ret ;
}


function show_form () {
	$ret .= "<form method='post'>" ;
	
	$ret .= "<table>" ;
	$ret .= "<tr><th>Breite</th>\n" ;
	$ret .= "<td>Grad</td><td>" . prefilled_input ( 'breite_grad' ) . "</td>\n" ;
	$ret .= "<td>Minute</td><td>" . prefilled_input ( 'breite_minute' ) . "</td>" ;
	$ret .= "<td>Sekunde</td><td>" . prefilled_input ( 'breite_sekunde' ) . "</td>" ;
	$ret .= "<td>" . prefilled_input ( 'breite_dir' , 'radio' , 'N' ) . "Nord " . prefilled_input ( 'breite_dir' , 'radio' , 'S' ) . "Süd</td>" ;
	$ret .= "</tr>" ;
	$ret .= "<tr><th>Länge</th>" ;
	$ret .= "<td>Grad</td><td>" . prefilled_input ( 'laenge_grad' ) . "</td>" ;
	$ret .= "<td>Minute</td><td>" . prefilled_input ( 'laenge_minute' ) . "</td>" ;
	$ret .= "<td>Sekunde</td><td>" . prefilled_input ( 'laenge_sekunde' ) . "</td>" ;
	$ret .= "<td>" . prefilled_input ( 'laenge_dir' , 'radio' , 'E' ) . "Ost " . prefilled_input ( 'laenge_dir' , 'radio' , 'W' ) . "West</td>" ;
	$ret .= "</tr>" ;
	$ret .= "<tr><th>Typ</th><td colspan='7'>" ;
	$ret .= "<select name='data_type'>" ;
	$ret .= prefilled_input ( 'data_type' , 'option' , 'country' , 'Land' ) ;
	$ret .= prefilled_input ( 'data_type' , 'option' , 'state' , 'Anderes Staatengebilde, nicht-souveräne Staaten' ) ;
	$ret .= prefilled_input ( 'data_type' , 'option' , 'adm1st' , 'Bundesländer, Kantone' ) ;
	$ret .= prefilled_input ( 'data_type' , 'option' , 'adm2nd' , 'Landkreise, Verwaltungsgemeinden' ) ;
	$ret .= prefilled_input ( 'data_type' , 'option' , 'city' , 'Stadt [evtl. Einwohnerzahl angeben!]' ) ;
	$ret .= prefilled_input ( 'data_type' , 'option' , 'isle' , 'Insel, Inselgruppe' ) ;
	$ret .= prefilled_input ( 'data_type' , 'option' , 'airport' , 'Flughafen, Luftwaffenstützpunkt' ) ;
	$ret .= prefilled_input ( 'data_type' , 'option' , 'mountain' , 'Berge, Hügel, Gebirgsketten [Höhe angeben!]' ) ;
	$ret .= prefilled_input ( 'data_type' , 'option' , 'waterbody' , 'Flüsse, Seen, Kanäle, Wasserfälle, Geysire' ) ;
	$ret .= prefilled_input ( 'data_type' , 'option' , 'landmark' , 'Sehenswürdigkeiten, Landmarken, Gebäude, alles andere' ) ;
	$ret .= "</select></td></tr><tr><td/><td colspan='6'>" ;
	$ret .= prefilled_input ( 'data_size' ) . "Einwohner bzw. Höhe</td></tr>" ;
	$ret .= "<tr><td/><td colspan='6'>Vorlage : " ;
	$ret .= prefilled_input ( 'template_name' , 'radio' , 'article=|' ) . "Ganzer Artikel" ;
	$ret .= prefilled_input ( 'template_name' , 'radio' , 'text=|name=…|' ) . "Im Text" ;
	$ret .= prefilled_input ( 'template_name' , 'radio' , 'text=|article=|' ) . "Beides" ;
	$ret .= "</td><td><input name='doit' type='submit' value='OK' /></td></tr>" ;
	$ret .= "</table>" ;
	
	$ret .= "</form>" ;
	return $ret ;
}

print "<html><body>" ;
print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head><body>' ;
print get_common_header ( 'coord_gen.php' ) ;

print process_form () ;
print show_form () ;

print "</body></html>" ;

?>