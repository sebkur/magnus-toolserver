<?php

error_reporting ( E_ALL ) ;
@set_time_limit ( 15*60*60 ) ; # Time limit 5 h


include_once ( "queryclass.php") ;
high_mem ( 250 , 'most_wanted_encyclopaedia_topics' ) ;

$language = "en" ;
$project = "wikipedia" ;
$met_page = "Wikipedia:WikiProject_Missing_encyclopedic_articles" ;



print "<html><body>" ;
print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>' ;
print get_common_header ( "met.php" ) ;
print "<body><h1>Missing encyclopedia topics</h1>" ;

# Get links
$wq = new WikiQuery ( $language , $project ) ;
$data = $wq->get_links ( $met_page , 4 ) ;

# Separate topic pages
$topic_keywords = array (
  "topic",
  "list",
  "wanted",
//  "missing",
  "requested",
  "missing",
  "dictionary",
  "famous",
  "archive",
  "wikiproject",
) ;

$topiclist = array () ;
foreach ( $data AS $d ) {
  if ( $d["ns"] != 4 ) continue ;
  $lt = strtolower ( $d["title"] ) ;
  $found = false ;
  if ( false !== stripos ( $lt , "stub" ) ) continue ;
  if ( false !== stripos ( $lt , "blank" ) ) continue ;
  foreach ( $topic_keywords AS $t ) {
    if ( false === stripos ( $lt , $t ) ) continue ;
    $found = true ;
    break ;
  }
  if ( !$found ) continue ;
  $t = $d["title"] ;
  $topiclist[$t] = $t ;
}

# Find sub-topic pages
$topicpages = array() ;
foreach ( $topiclist AS $tl ) {
  if ( $tl == 'Wikipedia:WikiProject Missing encyclopedic articles/Most wanted' ) continue ; // No self-reinforcement...
  print "Trying $tl ... " ;
  $data = $wq->get_links ( $tl ) ;
  $cnt = 0 ;
  foreach ( $data AS $d ) {
    if ( $d["ns"] != 4 ) continue ;
    $title = $d["title"] ;
    if ( substr ( $title , 0 , strlen ( $tl ) ) != $tl ) continue ;
    $topicpages[$title] = $title ;
    $cnt++ ;
  }
  if ( $cnt == 0 ) {
    $topicpages[$tl] = $tl ;
    print "added directly<br/>" ;
  } else {
    print "added $cnt subpages<br/>" ;
  }
  myflush();
#  if ( count ( $topicpages ) > 5 ) break ; # TESTING
}

# Find topics
$topics = array() ;
$next1000 = 10000 ;
foreach ( $topicpages AS $tp ) {
  $data = $wq->get_links ( $tp ) ;
  foreach ( $data AS $l ) {
    if ( $l["ns"] != 0 ) continue ;
    $title = $l["title"] ;
    $title = str_replace ( 'Ã¢â‚¬â„¢' , "'" , $title ) ; // Hardcoded fix for [[Wikipedia:Stanford Archive answers]]
    if ( isset ( $topics[$title] ) ) $topics[$title]++ ;
    else $topics[$title] = 1 ;
  }
  if ( count ( $topics ) > $next1000 ) {
    print "$next1000 found<br/>" ;
    myflush();
    $next1000 += 10000 ;
  }
}
print count ( $topics ) . " topics found, " ; myflush();

foreach ( $topics AS $title => $num ) {
  $lt = ucfirst ( strtolower ( $title ) ) ;
  if ( $title != $lt && isset ( $topics[$lt] ) ) {
    $topics[$title] += $topics[$lt] ;
    unset ( $topics[$lt] ) ;
  }
  if ( $num > 1 ) continue ;
  unset ( $topics[$title] ) ; # Only once
}
print count ( $topics ) . " of which are wanted by more than one list.<br/>" ; myflush();

# Show
arsort ( $topics ) ;
$cnt = 0 ;
foreach ( $topics AS $title => $num ) {
	$lt = ucfirst ( strtolower ( $title ) ) ;
	$ex = db_get_existing_pages ( array ( $title , $lt ) , 'en' , 'wikipedia' ) ;
//	$ex = $wq->get_existing_pages ( array ( $title , $lt ) ) ;
	if ( count ( $ex ) > 0 ) continue ; # Exists
	
	if ( false !== stripos ( $title , "school" ) ) continue ; # No school articles, pleeeease...
	if ( false !== stripos ( $title , ".com" ) ) continue ; # No websites
	if ( false !== stripos ( $title , "various authors" ) ) continue ; # Who links this shit?
	if ( false !== stripos ( $title , "journal" ) ) continue ; # No journals (usually too general)
	if ( false !== stripos ( $title , "academy" ) ) continue ; # No academies (usually too broad)
	
	if ( $cnt % 20 == 0 ) { print "<br/>== Section " . ( $cnt/20+1 ) . " ==<br/>" ; myflush(); }
	print "# [[$title]] ($num &times;)" ;
	if ( $title != $lt ) print " or [[$lt]]" ;
	print " {{search|" . str_replace ( " " , "+" , $title ) . "}}<br/>" ;
	$cnt++ ;
	if ( $cnt >= 500 ) break ; # First 500
}

print "</body></html>" ;


?>